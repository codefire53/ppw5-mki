"""story5 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mainapp.views import activity_input,activity_show,delete_all,about,skills,projects,experiences,contact,education,landing,delete
urlpatterns = [
    path('admin/', admin.site.urls),
    path('about/',about,name='about'),
    path('skills/',skills,name='skills'),
    path('projects/',projects,name='projects'),
    path('experiences/',experiences,name='experiences'),
    path('contact/',contact,name='contact'),
    path('education/',education,name='education'),
    path('',landing,name='landing'),
    path('activity/input',activity_input,name='activity_input'),
    path('activity/delete/',delete_all,name='delete'),
    path('activity/',activity_show,name='activity_show'),
    path('/activity/delete/<int:item_id>/',delete,name='delete_obj')
]
