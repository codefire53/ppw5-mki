from django.db import models

# Create your models here.
class Activity(models.Model):
    CATEGORY = (
        ('Kuliah', 'Kuliah'),
        ('Magang', 'Magang'),
        ('Ngasdos', 'Ngasdos'),
        ('Liburan', 'Liburan')
    )
    name = models.CharField(max_length=50)
    place = models.CharField(max_length=60)
    category=models.CharField(max_length=8,choices=CATEGORY)
    start_date=models.DateField()
    end_date=models.DateField()
    description=models.TextField()
