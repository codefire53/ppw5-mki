from django.shortcuts import render, redirect
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from .forms import ActivityForm
from .models import Activity
from datetime import datetime
import json
# Create your views here.
json_dict=dict()
def about(request):
    return render(request,'about2.html')
def skills(request):
    return render(request,'skill2.html')
def projects(request):
    return render(request,'project2.html')
def experiences(request):
    return render(request,'experience2.html')
def education(request):
    return render(request,'education2.html')
def contact(request):
    return render(request,'contact2.html')
def landing(request):
    return render(request,'landing.html')
    
def activity_input(request):
    if request.method=='POST':
        data=ActivityForm(request.POST or None)
        if data.is_valid():
            data.save()
            data=ActivityForm()
            return render(request,'activity_input.html',{'form':data})
           
        json_dict['form']=data
        return render(request, "activity_input.html", {'form': json_dict['form']})
    else:
        data=ActivityForm()
        return render(request, "activity_input.html", {'form': data})
    
def activity_show(request):
    activity=Activity.objects.all()
    json_dict['form']=activity
    return render(request,'activity_show.html',{'form': json_dict['form']})

def delete_all(request):
    if(Activity.objects.all().exists()):
        Activity.objects.all().delete()
        json_dict['form']=Activity.objects.all()
    return redirect('/activity/')
def delete(request,item_id =None):
    object = Activity.objects.get(id=item_id)
    object.delete()
    return redirect('/activity/')

