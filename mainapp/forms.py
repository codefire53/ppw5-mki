from django.forms import ModelForm, DateInput
from .models import Activity
class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        fields = ['name','place','category','start_date','end_date','description']
        widgets = {
        'start_date': DateInput(format=('%m/%d/%Y'), attrs={'class':'form-control', 'placeholder':'Select a date', 'type':'date'}), 
        'end_date': DateInput(format=('%m/%d/%Y'), attrs={'class':'form-control', 'placeholder':'Select a date', 'type':'date'})
        }
    def __init__(self, *args, **kwargs):
        super(ActivityForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })


